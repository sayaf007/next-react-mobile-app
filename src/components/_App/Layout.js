import Head from "next/head";

import HeadContent from "./HeadContent";
import Navbar from "./Navbar";

function Layout({ children }) {
  return (
    <>
      <Head>
        <HeadContent />
        {/* Stylesheets */}
        {/* <link
          rel="stylesheet"
          type="text/css"
          href="../../../static/styles.css"
        /> */}
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet" />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
        />
        <link
          rel="stylesheet"
          href="https://fonts.googleapis.com/icon?family=Material+Icons"
        />
        <title>App</title>
      </Head>

      <div className="max-w-sm container mx-auto">
        <Navbar />
        <div className="">
          {children}
        </div>
      </div>
    </>
  );
}

export default Layout;
