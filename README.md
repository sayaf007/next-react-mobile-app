# Next/Redux/Sagas/Tailwind

### Deployed on Vercel

- [Live URL](https://next-react-mobile-app.vercel.app/) https://next-react-mobile-app.vercel.app/

![App](/uploads/ec7cfd178bacf82817182398c915f249/Capture1.JPG)


### Dependencies

---

- [React](https://github.com/facebook/react): A declarative, efficient, and flexible JavaScript library for building user interfaces.
- [Next](https://github.com/zeit/next.js): CSS Library.
- [Tailwind CSS](https://github.com/vercel/next.js/tree/canary/examples/with-tailwindcss): The React Framework.
- [Redux](https://github.com/reduxjs/redux): A predictable state container for JavaScript apps.
- [Redux-Saga](https://github.com/redux-saga/redux-saga): An alternative side effect model for Redux apps.
- [Next-Redux-Wrapper](https://github.com/kirill-konshin/next-redux-wrapper): Redux wrapper for Next.js
- [axios](https://github.com/axios/axios): Promise based HTTP client for the browser and node.js

### DevDependencies

---

- [Logger for Redux](https://github.com/LogRocket/redux-logger): Logger for Redux

### Getting Started

---

#### Dev

```bash
$ npm install && npm run dev
```
#### Build

```bash
$ npm run build
```

#### Mock Data
```
 //Used in recent experiences
 const recentExperiences = [
  {
    "image": "images/placeholder-1.svg",
    "title": "Defi Swap",
    "descriptiom": "Swap your digital assets",
    "users": "+200"
  },
  {
    "image": "images/placeholder-2.svg",
    "title": "Docu sign",
    "descriptiom": "Sign smart contracts seamlessly",
    "users": "+200"
  }
]
```
![Homepage](/uploads/ec7cfd178bacf82817182398c915f249/Capture1.JPG)

```
 //Used in popular Categories and filter experiences
 const experiences = [
    {
        "id": 1,
        "image": "images/exchange-icon.svg",
        "title": "Exchanges"
    },
    {
        "id": 2,
        "image": "images/games-icon.svg",
        "title": "Games",
    },
    {
        "id": 3,
        "image": "images/marketplace-icon.svg",
        "title": "Marketplaces"
    },
    {
        "id": 4,
        "image": "images/defi-icon.svg",
        "title": "Defi"
    },
    {
        "id": 5,
        "image": "images/collectibles-icon.svg",
        "title": "Collectibles"
    },
    {
        "id": 6,
        "image": "images/utilities-icon.svg",
        "title": "Utilities"
    }
]
```
![Experiences](/uploads/5e0193abf4d598aa62dd47a4eb18a1df/Capture2.JPG)

```
//Used in acounts picker
const accounts = [
    {
        "id": 1,
        "avatar": "images/avatar-2.svg",
        "name": "johndoe.near",
        "amount": 0.34
    },
    {
        "id": 2,
        "avatar": "images/avatar-2.svg",
        "name": "mike.near",
        "amount": 0.12
    },
    {
        "id": 3,
        "avatar": "images/avatar-2.svg",
        "name": "john.near",
        "amount": 2.34
    }
]

```
![Accounts](/uploads/2cc0e3ae217645d3dc7a81f8f973c207/Capture3.JPG)

### Deployment process
To deploy your Next.js + Userbase app with Vercel for Git, make sure it has been pushed to a Git repository.

During the import process, you will need to add the following environment variable:

NEXT_PUBLIC_USERBASE_APP_ID
Import the project into Vercel using your Git provider of choice:

GitHub
GitLab
Bitbucket
After your project has been imported, all subsequent pushes to branches will generate Preview Deployments, and all changes made to the Production Branch (commonly "main") will result in a Production Deployment.