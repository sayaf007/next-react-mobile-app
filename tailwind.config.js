module.exports = {
  purge: ['./src/pages/**/*.{js,ts,jsx,tsx}',
  './src/components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        "gray-primary": "#F5F5F5",
        "primary": "#587BE0"
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: []
}